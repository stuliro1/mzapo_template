#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "queue.h"

#define REALLOC_MARGIN 20
#define INITIAL_SIZE 30

typedef struct{
    int size;
    void** data;
    queue_t* indexes;
}buffer_t;

buffer_t* create_buffer();

int add_to_buffer(buffer_t* buffer, void* data);

void* get_element(buffer_t* buffer, int index);

buffer_t* realloc_buffer(buffer_t* buffer);

void push_to_indexes(buffer_t* buffer, int start_index);

int buffer_elements_count(buffer_t* buffer);

int find_element(void* element);

void* inspect_element(buffer_t* buffer, int index);