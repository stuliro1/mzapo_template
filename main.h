#ifndef __INC
#include "queue.h"
#include "buffer.h"
#define __INC

typedef struct{
    queue_t* servers_to_draw;
    queue_t* servers_to_shut;
    buffer_t* servers;
}com_buffers;

typedef struct{
    int id;
    unsigned char name[10];
    int session_count;
    struct sockaddr* addr;
}server_t;
#endif
