#include <stdio.h>

#include "queue.h"
#include "main.h"
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"

void* user_interaction_run(void *vargp);

void redraw();

void draw_new_server(int display_offset, server_t* server_index);

void draw_header();

void realloc_server_display();

struct sockaddr* remove_server();

void wipe_server_display();

void draw_character(unsigned short* buffer, int buffer_width, int position, unsigned char character, short color, char char_width);

void draw_text(unsigned short* buffer, int buffer_width, int position, unsigned char* text, int text_length, short color);

void number_to_chars(unsigned char* arr, int number);
