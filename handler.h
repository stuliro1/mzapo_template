#include "networking.h"
#include <string.h>

void* handle_packets(void* args);

void response(void* packet, buffer_t* tbr_buffer);
void ping(void* packet);
void session_info(void* packet);
void session_create(void* packet, buffer_t* sessions);
void new_server(void* packet);
void new_client(void* packet);
void created_session(void* packet, buffer_t* sessions, buffer_t* tbr_buffer);
void session_info_request(void* packet, buffer_t* sessions);


int find_int_in_buffer(buffer_t* buffer, int element, int remove);
int find_rec_resp_in_buffer(buffer_t* buffer, int element, int remove);

void respond(void* packet, buffer_t* rrbuffer, int should_add_to_buffer);
void send_session_info(void* packet, void* session);