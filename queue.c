#include "queue.h"

#define BASE_LENGTH 10

static int realloc_data(queue_t* queue, double multipier);

queue_t* create_queue(int capacity){
    queue_t* created = (queue_t*)calloc(sizeof(queue_t), 1);
    if(!created){
        return NULL;
    }

    void** data = (void**)calloc(sizeof(void*), capacity);
    if(!data){
        free(created);
        return NULL;
    }

    created-> data = data;
    created-> start = 0;
    created-> end = 0;
    created-> size = capacity;
    
    return created; 
}

void delete_queue(queue_t *queue){
        free(queue-> data);
        free(queue);
}

bool push_to_queue(queue_t *queue, void *data){
    queue-> data[queue-> end] = data;

    ++queue-> end;
    if(queue-> end == queue-> size){
        queue-> end = 0;
    }
    if(queue-> end == queue-> start){
        realloc_data(queue, 2);
    }

    return 1;
}

void* pop_from_queue(queue_t *queue){
    if(queue-> start >= queue-> size){
        queue-> start = 0;
    }
    if(queue-> start == queue-> end){
        return NULL;
    }
    //printf("pop start %d, end %d  size %u\n", queue-> start, queue->end, queue-> size);
    void* popped = queue-> data[queue->start];
    queue-> data[queue->start] = NULL;
    ++queue-> start;

    if(get_queue_size(queue) < queue-> size/2 && queue-> size >= BASE_LENGTH){
        realloc_data(queue, 0.5);
    }
    return popped;
}

void* get_from_queue(queue_t *queue, int idx){
    if(idx < 0 || idx >= queue-> size){
        return NULL;
    }
    int index = queue-> start + idx;
    if(index >= queue-> size)index = index - queue-> size;
    return queue-> data[index];
}

int get_queue_size(queue_t *queue){
    if(queue-> end < queue-> start){
        return (queue->end + queue-> size) - queue-> start;
    }
    return queue-> end - queue-> start;
}

static int realloc_data(queue_t* queue, double multipier){
    void** tmp = (void**)calloc(sizeof(void*), (queue-> size) * multipier);
    if(!tmp){
        return 0;
    }
    int size = queue-> size;
    if(multipier < 1){
        size *= multipier;
    }
    for(size_t i = 0, j = queue-> start; i < size; ++i, ++j){
        if(j == queue-> size){
            j = 0;
        }
        tmp[i] = queue-> data[j];
    }
    if(multipier > 1){
        queue-> end = queue-> size;
    }else{
        queue-> end = get_queue_size(queue);
    }
    queue-> start = 0;
    free(queue-> data);
    queue-> data = tmp;
    queue-> size *= multipier;
    return 1;
}

