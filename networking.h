#include <stdio.h>
#include <sys/socket.h>

#include <arpa/inet.h>
#include <pthread.h>

#ifndef __INCLUDED
    #define __INCLUDED
    #include "queue.h"
    #include "main.h"
    #include "handler.h"


#define MESSAGE_LENGTH 2048
#define DEFAULT_BUFFERS_SIZE 50

#define RESPONSE 0
#define PING 1
#define NEW_DATA_STREAM 2
#define DATA_STREAM_DATA 3
#define DATA_STREAM_RESPONSE 4
#define DATA_RESPONSE_PACKET 5

#define NEW_SESSION_SERVER 8
#define NEW_SESSION_REQUEST 9
#define SESSION_INFO_REQUEST 10
#define NEW_CLIENT 11
#define CREATED_SESSION 12

#define START_NEW_SESSION 32

#define CLIENT_INFO 64
#define SESSION_INFO 65

typedef struct{
    struct sockaddr sender_addr;
    socklen_t client_struct_length;
    struct __attribute__((__packed__)){
        char type;
        int32_t UID;
        union
        {
            char data[MESSAGE_LENGTH-5];
            struct{
                char name[10];
            }new_server;
            struct{
                char name_length;
                char password_length;
                char password[];
            }session_create;
            struct {
                int id;
                char name_length;
                char password_length;
                char password[];
            }start_session;
            struct{
                int address;
                int port;
                int sessionID;
                char password_length;
                char password[];
                // char name_length;
                // char name[0];
            }session_info;
            uint64_t client_id;
            struct{
                int id;
                char password_length;
                char password[];
            }session_info_request;
        };

    }message;
}packet_t;

typedef struct{
    int* run;
    int* socket_desc;
    buffer_t* rr_buffer;
    buffer_t* tbr_buffer;
    queue_t* received;
    buffer_t* sessions;
}networking_info;

typedef struct{
    int id;
    char created;
    server_t* server;
    char password_length;
    char password[];
}session_t;

typedef struct{
    int UID;
    time_t expiration_time; 
}rec_resp;

typedef struct
{
    int id;
    time_t expiration_time;
    struct sockaddr* addr;
    unsigned char* data;
    int message_length;
}rec_send;

union packet_start{
    struct{
        char type;
        int UID;
    }parts;
    char data[5];
};

#endif

void* start_networking(void* args);

void* control_thread(void* args);

void send_packet(char* msg,int msg_len,struct sockaddr* dest_addr, int should_add_to_buffer);

void add_server_to_ui();

void check_rr_buffer(buffer_t* buffer);

void check_tbr_buffer(buffer_t* buffer);

void check_servers_to_shut();

server_t* choose_session_server();