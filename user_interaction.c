#include "user_interaction.h"
#include "main.h"
#include "font_types.h"
#include <time.h>

#define NUMBER_TEXT_LENGTH 10

#define SERVER_DISPLAY_AREA 100*450

#define SERVER_WIDTH 450
#define SERVER_HEIGHT 100

#define SCROLL_WIDTH 30

#define DISPLAY_WIDTH 480
#define HEADER_TITLE_START 5*DISPLAY_WIDTH+20
#define HEADER_SERVER_COUNT_START 5*DISPLAY_WIDTH + 250
#define HEADER_SERVER_COUNT_NUMBERS 5*DISPLAY_WIDTH + 350

#define NAME_START_POSITION 15*SERVER_WIDTH+30
#define SESSIONS_START_POSITION 50*SERVER_WIDTH+30
#define SESSION_COUNT_NUMBER_START 50*SERVER_WIDTH+120

#define PAGE_KNOB_VALUE *(volatile uint32_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o) & 0xff
#define SELECT_KNOB_VALUE (*(volatile uint32_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o) >> 8) & 0xff
#define DELETE_BUTTON (*(volatile uint32_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o) >> 25) & 0x1



int UI_run = 1;
unsigned char *parlcd_mem_base;
unsigned char *mem_base;
int scroll_offset = 0, pages = 0, allocated_pages = 1, selected = 0, to_delete = 0, delete_button_released = 1;
unsigned long delete_button_press_time = 0;
unsigned short* header;
unsigned short** servers_display;
server_t** server_table_desc;
buffer_t* servers;
font_descriptor_t* fdes;

void* user_interaction_run(void *vargp){

    printf("-  UI\n");

    fdes = &font_winFreeSystem14x16;
        
    header = (unsigned short*)malloc(20*480*2);
    servers_display = (unsigned short**)malloc(3* sizeof (short*));
    server_table_desc = (server_t**)malloc(3* sizeof(server_t*));
    for(int i = 0; i < 3; ++i){
        servers_display[i] = (unsigned short*)calloc(SERVER_DISPLAY_AREA, 2);
    }

    // int num = 1234;
    // unsigned char num_chars[8];
    // number_to_chars(num_chars, num);
    // draw_text(servers_display[0], SERVER_WIDTH, 0, num_chars, 8, 0x65);

    // for(int y = 0; y < 16; y++){
    //     for(int x = 0; x < 40; x++){
    //         printf("%c_", servers_display[0][y*SERVER_WIDTH+x]);
    //     }
    //     printf("\n");
    // }

    volatile com_buffers* com = (com_buffers*)vargp;

    servers = com->servers;

    parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);

    mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);

    parlcd_hx8357_init(parlcd_mem_base);

    draw_header();
    redraw();

    while(UI_run){
        //check for knob turn
        uint32_t new_offset = (PAGE_KNOB_VALUE*pages)/256;
        uint32_t select_knob = SELECT_KNOB_VALUE;
        int redraw_b = 0;
        if(new_offset != scroll_offset){
            to_delete = 0;
            scroll_offset = new_offset;
            //TODO update page counter
            redraw_b = 1;
        }
        if((select_knob*3)/256 != selected){
            to_delete = 0;
            selected = (select_knob*3)/256;
            redraw_b = 1;
            *(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = 1 << (scroll_offset+selected);
            //TODO light LED based on location
        }
//TODO fix it didnt work, wasnt detected at press  ut at scroll and crashed the program
        //check for knob press
        if(DELETE_BUTTON && delete_button_released){
            delete_button_released = 0;
            delete_button_press_time = time(NULL);
            if(to_delete){
                struct sockaddr* deleted_addr = remove_server();
                push_to_queue(com->servers_to_shut, (void*)(deleted_addr));
                to_delete = 0;
            }else{
                to_delete = 1;
            }
            redraw_b = 1;
        }
        if(!DELETE_BUTTON && time(NULL) > (delete_button_press_time + 1)){
            delete_button_released = 1;
        }

        //check for new server
        int ser = (int)pop_from_queue(com->servers_to_draw);
        if(ser != NULL){
            if(allocated_pages*3 == buffer_elements_count(servers)){
                realloc_server_display();
            }
            // add_to_buffer(servers, (void*)ser);
            int display_Index = buffer_elements_count(servers)-1;
            if(display_Index%3 == 0){
                pages++;
            }
            draw_header();
            server_table_desc[display_Index] = ser-1;
            draw_new_server(display_Index, inspect_element(com->servers, ser-1));
            if(display_Index >= scroll_offset*3 && display_Index < (scroll_offset+1)*3){
                redraw_b = 1;
            }
        }

        //TODO check for check for server update
 
        if(redraw_b){
            redraw();
        }
    }
    return NULL;
}

void redraw(){
    //draw header
    draw_header();
    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for(int i = 0; i < 20*480; ++i){
        parlcd_write_data(parlcd_mem_base, header[i]);
    }

    //draw servers

    for(int i = 0; i < 3; ++i){
        //TODO draw selector
        for(int y = 0; y < SERVER_HEIGHT; y++){
            for(int x = 0; x < SERVER_WIDTH; x++){
                parlcd_write_data(parlcd_mem_base, servers_display[i+(scroll_offset*3)][y*SERVER_WIDTH+x]);
            }
            unsigned short color;
            if(selected == i){
                if(to_delete){
                    color = 0xf800;
                }else{
                    color = 0x07e0;
                }
            }else{
                color = 0x0;
            }
            for(int sc = 0; sc < SCROLL_WIDTH; sc++){
                parlcd_write_data(parlcd_mem_base, color);
            }
        }
    }
}

void draw_new_server(int display_index, server_t* ser){
    //Set Background and border
    for(int y = 0; y < SERVER_HEIGHT; y++){
        for(int x = 0; x < SERVER_WIDTH; x++){
            if(y == 0 || y == SERVER_HEIGHT-1 || x == 0 || x == SERVER_WIDTH-1){
                servers_display[display_index][y*SERVER_WIDTH + x] = 0xffff;
            }else{
                servers_display[display_index][y*SERVER_WIDTH + x] = 0;
            }
        }
    }

    //draw default text
    draw_text(servers_display[display_index], SERVER_WIDTH, NAME_START_POSITION, ser->name, 10, 0xffff);
    draw_text(servers_display[display_index], SERVER_WIDTH, SESSIONS_START_POSITION, "SESSIONS:", 9, 0xffff);

    //TODO drawn number of sessions
    char session_count[8];
    number_to_chars(session_count, ser->session_count);
    draw_text(servers_display[display_index], SERVER_WIDTH, SESSION_COUNT_NUMBER_START, session_count, 8, 0xffff);
}

void draw_header(){
    draw_text(header, DISPLAY_WIDTH, HEADER_TITLE_START, "NP Login Server", 15, 0xffff);
    
    draw_text(header, DISPLAY_WIDTH, HEADER_SERVER_COUNT_START, "Servers:", 8, 0xffff);

    
    int server_count = buffer_elements_count(servers);
    unsigned char number_chars[NUMBER_TEXT_LENGTH];
    number_to_chars(number_chars, server_count);
    draw_text(header, DISPLAY_WIDTH, HEADER_SERVER_COUNT_NUMBERS, number_chars, NUMBER_TEXT_LENGTH, 0xffff);
}

void realloc_server_display(){
    servers_display = (unsigned short**)realloc(servers_display, ((allocated_pages+1)*3)*sizeof(short*));
    server_table_desc = (server_t**)realloc(server_table_desc, ((allocated_pages+1)*3)*sizeof(short*));
    for(int i = 0; i < 3; ++i){
        servers_display[allocated_pages*3 + i] = (unsigned short*)calloc(SERVER_DISPLAY_AREA, 2);
    }
    allocated_pages++;
}

struct sockaddr* remove_server(){
    wipe_server_display();
    server_t* server = get_element(servers, (int)server_table_desc[scroll_offset*3+selected]);
    if(scroll_offset*3+selected == buffer_elements_count(servers)){
        return server->addr;
    }

    //shifting other servers upward
    unsigned short* wiped_server = servers_display[scroll_offset*3+selected];
    for(int i = scroll_offset*3+selected; i < buffer_elements_count(servers)-1; i ++){
        servers_display[i] = servers_display[i+1];
        server_table_desc[i] = server_table_desc[i+1];
    }
    servers_display[buffer_elements_count(servers)-1] = wiped_server;
    server_table_desc[buffer_elements_count(servers)-1] = NULL;
    return server->addr;
}

void wipe_server_display(){
    for(int i = 0; i < SERVER_DISPLAY_AREA; i++){
        servers_display[scroll_offset*3+selected][i] = 0;
    }
}

void draw_character(unsigned short* buffer, int buffer_width, int position, unsigned char character, short color, char char_width){
    for(int y = 0; y < 16; y++){
        for(int x = 0; x < char_width; x++){
            buffer[position+ y*buffer_width + x] = color * ((fdes->bits[character*16+y]>>(15-x)) & 0x1);
            // printf("%c ", buffer[position+ y*buffer_width + x]);
        }
        // printf("\n");
    }
}

void draw_text(unsigned short* buffer, int buffer_width, int position, unsigned char* text, int text_length, short color){
    char char_width;
    for(int i = 0; i < text_length; i++){
        if(text[i] == '\0'){
            break;
        }
        char_width = fdes->width[text[i]-32];
        draw_character(buffer, buffer_width, position, text[i]-32, color, char_width);
        position += char_width;
    }
}

void number_to_chars(unsigned char* arr, int number){
    unsigned char reversed_chars[NUMBER_TEXT_LENGTH];
    int i = 0;
    if(number == 0){
        arr[0] = '0';
        i++;
    }else{
        while(number != 0){
        reversed_chars[i] = (number % 10)+'0';
        number /= 10;
        i++;
        }
        for(int x = 0; x <= i; x++){
            arr[x] = reversed_chars[i-x-1];
        }
    }

    arr[i] = '\0';
}

