#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>

#include "mzapo_parlcd.h"
#include "user_interaction.h"
#include "networking.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "serialize_lock.h"
#include "main.h"



int main(int argc, char *argv[])
{
  printf("Starting\n");
  
  com_buffers com;
  
  com.servers_to_draw = create_queue(10);
  com.servers_to_shut = create_queue(10);
  com.servers = create_buffer();
  //start UserInteraction thread
  pthread_t UI;
  pthread_create(&UI, NULL, user_interaction_run, (void*)&com);

  //start networking thread
  pthread_t networking;
  pthread_create(&networking, NULL, start_networking, (void*)&com);
  getchar();
  printf("Ending\n");
}
