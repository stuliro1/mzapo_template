#include "buffer.h"

buffer_t* create_buffer(){
    buffer_t* buffer = (buffer_t*) malloc (sizeof(buffer_t));
    buffer->size = INITIAL_SIZE;
    buffer->data = (void**) calloc (INITIAL_SIZE, sizeof(void*));
    buffer->indexes = create_queue(INITIAL_SIZE);
    push_to_indexes(buffer, 0);
    return buffer;
}

int add_to_buffer(buffer_t* buffer, void* packet){
    int index = (int)(pop_from_queue(buffer->indexes));
    if(get_queue_size(buffer->indexes) > 0){
        buffer->data[index] = packet;
    }else{
        realloc_buffer(buffer);
        add_to_buffer(buffer, packet);
    }
    return index;
}

void* get_element(buffer_t* buffer, int index){
    push_to_queue(buffer->indexes, (void*)&index);
    void* ret = buffer->data[index];
    buffer->data[index] = NULL;
    return ret;
}

// packet_t* get_packet_UID(buffer_t* buffer, int UID){
//     for(int i = 0; i < buffer->indexes->size; ++i){
//         if(UID == buffer->data->message.UID){
//             return get_packet_index(buffer, i);
//         }
//     }
// }

buffer_t* realloc_buffer(buffer_t* buffer){
    buffer = (buffer_t*) realloc(buffer, buffer->size+REALLOC_MARGIN);
    buffer->size += REALLOC_MARGIN;
    push_to_indexes(buffer, buffer->size - REALLOC_MARGIN);
    return buffer;
}

void push_to_indexes(buffer_t* buffer, int start_index){
    for(int i = start_index; i < buffer->size; ++i){
        push_to_queue(buffer->indexes, (void*)i);
    } 
}

int buffer_elements_count(buffer_t* buffer){
    return buffer->size-get_queue_size(buffer->indexes);
}

void* inspect_element(buffer_t* buffer, int index){
    return buffer->data[index];
}

