#include "handler.h"

void* handle_packets(void* args){
    networking_info* info = (networking_info *)args;
    packet_t* packet;

    while(*info->run){
        //take from buffer
        packet = (packet_t *)pop_from_queue(info->received);
        if(packet == NULL){
            continue;
        }

        if(find_rec_resp_in_buffer(info->rr_buffer, packet->message.UID, 0)){
            // printf("responding again\n");
            respond(packet, info->rr_buffer, 0);
            continue;
        }

        switch (packet->message.type)
        {
        case RESPONSE:
            response((packet_t*)packet, info->rr_buffer);
            break;
        case NEW_SESSION_SERVER:
            new_server(packet);
            respond(packet, info->rr_buffer, 1);
            break;
        case NEW_SESSION_REQUEST:
            printf("new session create request\n");
            session_create(packet, info->sessions);
            break;
        case NEW_CLIENT:
            new_client(packet);
            break;
        case CREATED_SESSION:
            created_session(packet, info->sessions, info->tbr_buffer);
            respond(packet, info->rr_buffer, 0);
        case SESSION_INFO_REQUEST:
            session_info_request(packet, info->sessions);
        default:
            printf("default %d\n", packet->message.type);
            break;
        }
    }
    return NULL;
}

void response(void* packet, buffer_t* tbr_buffer){
    find_int_in_buffer(tbr_buffer, ((packet_t*)packet)->message.UID, 1);
}

int find_int_in_buffer(buffer_t* buffer, int element, int remove){
    for(int i = 0; i < buffer->size; ++i){
        // if(buffer->data[i] != NULL){
        //     printf("UID %d controlling %d\n", element, *(int*)(buffer->data[i]));
        // }
        if(buffer->data[i] != NULL && *(int*)(buffer->data[i]) == element){
            if(remove){
                return *(int*)get_element(buffer, i);
            }else{
                return 1;
            }
        }
    }
    return 0;
}

int find_rec_resp_in_buffer(buffer_t* buffer, int element, int remove){
    for(int i = 0; i < buffer->size; ++i){
        // if(buffer->data[i] != NULL){
        //     printf("UID %d controlling %d\n", element, *(int*)(buffer->data[i]));
        // }
        if(buffer->data[i] != NULL && ((rec_resp*)(buffer->data[i]))->UID == element){
            if(remove){
                return *(int*)get_element(buffer, i);
            }else{
                return 1;
            }
        }
    }
    return 0;
}

session_t* find_session_in_buffer(buffer_t* sessions, int ID){
    for(int i = 0; i < sessions->size; ++i){
        // if(buffer->data[i] != NULL){
        //     printf("UID %d controlling %d\n", element, *(int*)(buffer->data[i]));
        // }
        if(sessions->data[i] != NULL && ((session_t*)(sessions->data[i]))->id == ID){
            return (session_t*)inspect_element(sessions, i);
        }
    }
    return NULL;  
}

void new_server(void* packet){
    //TODO add server to UI
    printf("new session server, name: %s\n", ((packet_t*)packet)->message.new_server.name);
    add_server_to_ui(((packet_t*)packet)->message.new_server.name, &((packet_t*)packet)->sender_addr);
}

void respond(void* packet, buffer_t* rrbuffer, int should_add_to_buffer){
    if(should_add_to_buffer){
        rec_resp* resp = (rec_resp*)malloc(sizeof(rec_resp));
        resp->UID = ((packet_t*)packet)->message.UID;
        resp->expiration_time = time(NULL) + 2000;
        add_to_buffer(rrbuffer, (void*)resp);
    }
    ((packet_t*)packet)->message.type = 0;
    send_packet((char *)&((packet_t*)packet)->message, 5, &(((packet_t*)packet)->sender_addr), 0);
}

void session_create(void* packet, buffer_t* sessions){
    //look into sessions for packet UID
    int packet_uid = ((packet_t*)packet)->message.UID;
    session_t* session = find_session_in_buffer(sessions, packet_uid);
    if(session != NULL){
        if(session->created){
            send_session_info(packet, (void*)session);
        }
        return; 
    }
    session = (session_t*)malloc(sizeof(session_t));
    session->id = packet_uid;
    session->created = 0;
    strcpy(session->password, ((packet_t*)packet)->message.session_create.password);
    session->password_length = ((packet_t*)packet)->message.session_create.password_length;

    server_t* server = choose_session_server();
    session->server = server;
    add_to_buffer(sessions, (void*)session);

    packet_t packet_to_send;
    packet_to_send.message.type = START_NEW_SESSION;
    packet_to_send.message.UID = packet_uid;
    packet_to_send.message.start_session.id = packet_uid;
    packet_to_send.message.start_session.name_length = 0;
    packet_to_send.message.start_session.password_length = session->password_length;
    strcpy(packet_to_send.message.start_session.password, session->password);
    send_packet((char*)&packet_to_send.message, 11 + packet_to_send.message.start_session.password_length, server->addr, 1);
}

void send_session_info(void* packet, void* session){
    printf("sending session info\n");
    packet_t packet_to_send;
    packet_to_send.message.type = SESSION_INFO;
    packet_to_send.message.UID = ((packet_t*)packet)->message.UID;
    packet_to_send.message.session_info.address = ((((short*)(((session_t*)session)->server->addr->sa_data))[1] & 0x0000ffff)| (((int)((short*)(((session_t*)session)->server->addr->sa_data))[2])<<16 & 0xffff0000));
    packet_to_send.message.session_info.port = ((int)((short*)(((session_t*)session)->server->addr->sa_data))[0])<<16;
    packet_to_send.message.session_info.sessionID = ((session_t*)session)->id;
    packet_to_send.message.session_info.password_length = ((session_t*)session)->password_length;
    strcpy(packet_to_send.message.session_info.password, ((session_t*)session)->password);

    send_packet((char*)&packet_to_send.message, 18 + ((session_t*)session)->password_length, &(((packet_t*)packet)->sender_addr), 0);
}

void session_info_request(void* packet, buffer_t* sessions){
    session_t* session = find_session_in_buffer(sessions, ((packet_t*)packet)->message.session_info_request.id);
    if(session == NULL){
        return;
    }
    if(strcmp(session->password, ((packet_t*)packet)->message.session_info_request.password)){
        send_session_info(packet, session);
    }
}

void created_session(void* packet, buffer_t* sessions, buffer_t* tbr_buffer){
    printf("info about created session\n");
    int uid = ((packet_t*)packet)->message.UID;
    
    find_int_in_buffer(tbr_buffer, uid, 1);

    session_t* session = find_session_in_buffer(sessions, uid);
    session->created = 1;
}

void new_client(void* packet){
    packet_t packet_to_send;
    packet_to_send.message.type = CLIENT_INFO;
    packet_to_send.message.UID = ((packet_t*)packet)->message.UID;
    uint64_t client_id = 
        ((uint64_t)(((short*)(((packet_t*)packet)->sender_addr.sa_data))[2])<<16 & 0xffff0000) | 
        ((uint64_t)(((short*)(((packet_t*)packet)->sender_addr.sa_data))[1]) & 0xffff)|
        ((uint64_t)(((short*)(((packet_t*)packet)->sender_addr.sa_data))[0])<<48 & 0xffff000000000000);
    packet_to_send.message.client_id = client_id;
    send_packet((char*)&packet_to_send.message, sizeof packet_to_send.message, &(((packet_t*)packet)->sender_addr), 0);
}