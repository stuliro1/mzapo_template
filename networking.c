#include "networking.h"
#include <errno.h>
#include <string.h>

int socket_desc;
com_buffers* com;
networking_info info;
int run = 1;

void* start_networking(void* args){
    printf("-  networking\n");
    com = (com_buffers*)args;
    //create buffers (rr, tbr, received)
    info.run = &run;
    info.socket_desc = &socket_desc;
    info.rr_buffer = create_buffer();
    info.tbr_buffer = create_buffer();
    info.received = create_queue(DEFAULT_BUFFERS_SIZE);
    info.sessions = create_buffer();

    //start handler thread (pass buffers)
    pthread_t handler;
    pthread_create(&handler, NULL, handle_packets, (void *)&info);

    //start control thread (pass buffers)
    pthread_t control;
    pthread_create(&control, NULL, control_thread, (void *)&info);

    //create socket
    if((socket_desc = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1){
        printf("socket create failed\n");
        run = 0;
    }
    struct sockaddr_in server_addr;

    memset((char *) &server_addr, 0, sizeof(server_addr));
    
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(4400);
    server_addr.sin_addr.s_addr = inet_addr("192.168.223.130");

    if(bind(socket_desc, (struct sockaddr*)&server_addr, sizeof(server_addr)) < 0){
        // printf("%d\n", errno);
        printf("bind failed\n");
        run = 0;
    }

    //start receiving
    while(run){
        packet_t packet;
        int count = 0;
        if((count = recvfrom(socket_desc, (char*)(&packet.message), MESSAGE_LENGTH, 0,(struct sockaddr*)&packet.sender_addr, &(packet.client_struct_length))) < 0){
            run = 0;
            printf("-  receive failed\n");
            continue;
        }
        // printf("%d %d %d %d %d\n", ((char*)&packet.message)[0], ((char*)&packet.message)[1], ((char*)&packet.message)[2], ((char*)&packet.message)[3], ((char*)&packet.message)[4]);
        // printf("UID  %d\n", packet.message.UID);

        //printf("adding server port = %d\n", ((short*)(packet.sender_addr.sa_data))[0]);
        if(((short*)(packet.sender_addr.sa_data))[0] == 0){
            continue;
        }

        push_to_queue(info.received, &packet);
    }

    //free buffers
    return NULL;
}

void send_packet(char* msg,int msg_len,struct sockaddr* dest_addr, int should_add_to_buffer){
    ssize_t amount = sendto(socket_desc, msg, msg_len+1, 0, dest_addr, sizeof *dest_addr);
    if(should_add_to_buffer){
        rec_send send;
        send.data = msg;
        send.message_length = msg_len;
        send.expiration_time = time(NULL) + 1;
        send.addr = dest_addr;
        add_to_buffer(info.tbr_buffer, (void*)&send);
    }
}

void add_server_to_ui(unsigned char name[10], struct sockaddr* addr){
    printf("adding server to UI\n");
    server_t* ser = (server_t*)malloc(sizeof(server_t));
    strcpy(ser->name, name);
    ser->session_count = 0;

    struct sockaddr* alloced_adress = (struct sockaddr*)malloc(sizeof(struct sockaddr));
    alloced_adress->sa_family = addr->sa_family;
    memcpy(alloced_adress->sa_data, addr->sa_data, 6);

    ser->addr = alloced_adress;
    
    int added_index = add_to_buffer(com->servers, (void*)ser);
    push_to_queue(com->servers_to_draw, added_index+1);
}

void* control_thread(void* args){
    networking_info* info = (networking_info *)args;
    
    while(run){
        check_rr_buffer(info->rr_buffer);
        check_tbr_buffer(info->tbr_buffer);
        check_servers_to_shut();
    }
    return NULL;
}

void check_rr_buffer(buffer_t* buffer){
    time_t current = time(NULL);
    for(int i = 0; i < buffer->size; ++i){
        if(buffer->data[i] != NULL){
            if(((rec_resp*)buffer->data[i])->expiration_time < current){
                rec_send* s = (rec_send*)get_element(buffer, i);
                send_packet(s->data, s->message_length, s->addr, 1);
            }
        }
    }
}

void check_tbr_buffer(buffer_t* buffer){
    time_t current = time(NULL);
    for(int i = 0; i < buffer->size; ++i){
        if(buffer->data[i] != NULL){
            if(((rec_send*)buffer->data[i])->expiration_time < current){
                get_element(buffer, i);
            }
        }
    }
}

void check_servers_to_shut(){
    while(1){
        struct sockaddr* server_addr = (struct sockaddr*)pop_from_queue(com->servers_to_shut);
        if(server_addr == NULL){
            break;
        }
        printf("there is server to shut\n");
        // printf("adding server port = %d\n", ((short*)(packet.sender_addr.sa_data))[0]);
        union packet_start pac;
        pac.parts.type = 68;
        pac.parts.UID = rand();
        send_packet((char*)&pac, 5, server_addr, 1);
    }
}

server_t* choose_session_server(){
    // int server_index = rand() % (buffer_elements_count(com->servers)+1);
    // return (server_t*)get_element(com->servers, server_index);
    return (server_t*)inspect_element(com->servers, 0);
}

